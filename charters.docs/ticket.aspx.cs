﻿using System;
using System.Web;
using System.Data;

using charters.docs.INCLUDE.dataTableAdapters;

using iTextSharp.text;
using iTextSharp.text.pdf;

namespace charters.docs
{
    public partial class ticket : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string hash = Request.QueryString["hash"];
            int bookID = 0, bookStatus = 0, passID = 0;

            bool isPassID = Int32.TryParse(Request.QueryString["id"], out passID);

            _spCheckBookHash2_TA table = new _spCheckBookHash2_TA();
            var Rs = table.GetData(hash, false);

            BaseFont bf = BaseFont.CreateFont(Server.MapPath("/resources/arial.ttf"), BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            Font fontsText = new Font(bf, 12, Font.NORMAL, new CMYKColor(0, 0, 0, 255)); // Черный 12 px
            Font fontsText10 = new Font(bf, 10, Font.NORMAL, new CMYKColor(0, 0, 0, 255)); // Черный 10 px
            Font fontsTextSmallBold = new Font(bf, 9, Font.BOLD, new CMYKColor(0, 0, 0, 255)); // Черный 9 px жирный
            Font fontsTextSmall = new Font(bf, 9, Font.NORMAL, new CMYKColor(0, 0, 0, 150)); // Серый 9 px
            Font fontsParagraph = new Font(bf, 14, Font.BOLD, new CMYKColor(0, 0, 0, 255)); // Черный 12 px жирный

            Paragraph p1 = new Paragraph("", fontsParagraph);
            Paragraph p2 = new Paragraph("", fontsText);

            p1.Alignment = Element.ALIGN_CENTER;
            p2.Alignment = Element.ALIGN_CENTER;

            p1.SpacingAfter = 10;
            p2.SpacingAfter = 10;

            Phrase t1 = new Phrase("", fontsText);
            Phrase tSmall = new Phrase("", fontsTextSmall);

            var doc = new Document(PageSize.A4);
            PdfWriter.GetInstance(doc, Response.OutputStream);
            doc.Open();

            if (Rs.Rows.Count > 0)
            {
                bookID = (int)Rs.Rows[0]["BID"];
                bookStatus = Convert.ToInt32(Rs.Rows[0]["BStatusID"]);

                if (isPassID)
                {

                    if (bookID > 0 && bookStatus == 3)
                    {
                        // Пишем информацию о скачивании файла
                        new TAQuery()._spAddDownloadDocInfo(bookID, 2, passID, HttpContext.Current.Request.UserHostAddress);
                        Response.AppendHeader("content-disposition", "attachment; filename=ticket_" + passID.ToString() + ".pdf");

                        p1.Add("ЭЛЕКТРОННЫЙ БИЛЕТ (МАРШРУТ / КВИТАНЦИЯ)");
                        p2.Add("ELECTRONIC TICKET (ITINERARY / RECEIPT)");
                        doc.Add(p1);
                        doc.Add(p2);

                        // Добавляем таблицу с данными по брони
                        string bookNumber = "", pnr = "", ticketNumber = "", ticketDate = "", acName = "", passFio = "", docNo = "", passType = "";

                        _spGetBookPdfInfoTicket_TA tableBook = new _spGetBookPdfInfoTicket_TA();
                        var RsBook = tableBook.GetData(passID, bookID);
                        if (RsBook.Rows.Count > 0)
                        {
                            bookNumber = (string)RsBook.Rows[0]["BNumber"];
                            pnr = (string)RsBook.Rows[0]["pnr"];
                            ticketNumber = (string)RsBook.Rows[0]["ticketNumber"];
                            ticketDate = (string)RsBook.Rows[0]["ticketDate"];
                            acName = (string)RsBook.Rows[0]["acName"];
                            passFio = (string)RsBook.Rows[0]["lName"] + ' ' + (string)RsBook.Rows[0]["fName"] + ' ' + (string)RsBook.Rows[0]["mName"];
                            docNo = (string)RsBook.Rows[0]["docNo"];
                            passType = (string)RsBook.Rows[0]["CPType"];
                        }

                        PdfPTable tbl = new PdfPTable(2);

                        tbl.DefaultCell.Border = Rectangle.NO_BORDER;
                        tbl.WidthPercentage = 99;
                        tbl.SpacingBefore = 20;

                        tbl.AddCell(new Phrase("НОМЕР ЗАКАЗА / BILLING NUMBER:", fontsText10));
                        tbl.AddCell(new Phrase(bookNumber, fontsText10));

                        tbl.AddCell(new Phrase("ДАННЫЕ БРОНИ / BOOKING REF:", fontsText10));
                        tbl.AddCell(new Phrase(pnr, fontsText10));

                        tbl.AddCell(new Phrase("ДАТА ВЫПИСКИ / TICKET ISSUE DATE:", fontsText10));
                        tbl.AddCell(new Phrase(ticketDate, fontsText10));

                        tbl.AddCell(new Phrase("ВЫДАН ОТ / ISSUED BY:", fontsText10));
                        tbl.AddCell(new Phrase(acName, fontsText10));

                        tbl.AddCell(new Phrase("БРОНИРОВАНИЕ (RESERVATION NUMBER)*:", fontsText10));
                        tbl.AddCell(new Phrase(pnr, fontsText10));

                        doc.Add(tbl);

                        tSmall.Clear();
                        tSmall.Add("*код брони для онлайн-регистрации и других сервисов авиакомпании.");
                        doc.Add(tSmall);

                        // Добавляем таблицу с данными по пассажиру
                        PdfPTable tblPass = new PdfPTable(3);
                        tblPass.WidthPercentage = 99;
                        tblPass.SpacingAfter = 20;
                        tblPass.DefaultCell.Padding = 5;

                        tblPass.AddCell(new Phrase("ФАМИЛИЯ / NAME", fontsTextSmallBold));
                        tblPass.AddCell(new Phrase("ДОКУМЕНТ / DOCUMENT", fontsTextSmallBold));
                        tblPass.AddCell(new Phrase("НОМЕР БИЛЕТА / TICKET NUMBER", fontsTextSmallBold));

                        tblPass.AddCell(new Phrase(passFio, fontsText10));
                        tblPass.AddCell(new Phrase(docNo, fontsText10));
                        tblPass.AddCell(new Phrase(ticketNumber, fontsText10));

                        doc.Add(tblPass);

                        // Добавляем таблицу с данными по перелету
                        PdfPTable tblFlight = new PdfPTable(4);
                        tblFlight.WidthPercentage = 99;
                        tblFlight.SpacingAfter = 20;
                        tblFlight.DefaultCell.Padding = 5;

                        tblFlight.AddCell(new Phrase("ДАТА / РЕЙС / КЛАСС\nDATE / FLIGHT / CLASS", fontsTextSmallBold));
                        tblFlight.AddCell(new Phrase("ВЫЛЕТ\nDEPARTURE", fontsTextSmallBold));
                        tblFlight.AddCell(new Phrase("ПРИЛЁТ\nARRIVAL", fontsTextSmallBold));
                        tblFlight.AddCell(new Phrase("БАГАЖ\nBAGGAGE", fontsTextSmallBold));

                        _spGetBookFlightSetificate_TA tableFlight = new _spGetBookFlightSetificate_TA();
                        var RsFlight = tableFlight.GetData(bookID);
                        foreach (DataRow row in RsFlight.Rows)
                        {
                            string dateDep = (string)row["dateDep"];
                            string cityDep = (string)row["cityDep"];
                            string portDep = (string)row["portDep"];
                            string portDepCode = (string)row["portDepCode"];
                            string terminalDep = (string)row["terminalDep"];

                            string cityArr = (string)row["cityArr"];
                            string portArr = (string)row["portArr"];
                            string portArrCode = (string)row["portArrCode"];
                            string terminalArr = (string)row["terminalArr"];

                            string timeDep = (string)row["timeDep"];
                            string timeArr = (string)row["timeArr"];

                            string flName = (string)row["airCompany"] + " " + (string)row["flightNumber"];
                            string bortName = (string)row["CFT_Name"];
                            string classCode = (string)row["classCode"];
                            string baggage = (passType == "I") ? "0" : row["baggage"].ToString();

                            cityDep += (portDep != cityDep) ? ". " + portDep : "";
                            cityDep += " (" + portDepCode + ")";
                            cityDep += (terminalDep.Length > 0) ? ". Терминал " + terminalDep : "";

                            cityArr += (portArr != cityArr) ? ". " + cityArr : "";
                            cityArr += " (" + portArrCode + ")";
                            cityArr += (terminalArr.Length > 0) ? ". Терминал " + terminalArr : "";

                            tblFlight.AddCell(new Phrase(dateDep + "\n" + flName + "\n" + bortName + "\n" + classCode, fontsText10));
                            tblFlight.AddCell(new Phrase(cityDep + "\n" + timeDep, fontsText10));
                            tblFlight.AddCell(new Phrase(cityArr + "\n" + timeArr, fontsText10));
                            tblFlight.AddCell(new Phrase(baggage + "K", fontsText10));
                        }

                        doc.Add(tblFlight);

                        // Добавляем общий текст
                        tSmall.Clear();
                        tSmall.Add("ИНФОРМАЦИЯ ПО РЕГИСТРИРУЕМОМУ БАГАЖУ/CHECK-IN BAGGAGE INFORMATION 0N, 0P - НЕТ БАГАЖА; 1N, 1P – ОДНО МЕСТО БАГАЖА; K, KG – КГ; ВНИМАНИЕ!/ ATTENTION! УКАЗАНО МЕСТНОЕ ВРЕМЯ ВЫЛЕТА И ПРИБЫТИЯ/DEPARTURES AND ARRIVALS ARE IN LOCAL TIME; НАКАНУНЕ ВЫЛЕТА УТОЧНИТЕ ВРЕМЯ ВЫЛЕТА РЕЙСА/BEFORE DEPARTURE CHECK TIME OF DEPARTURE; РЕГИСТРАЦИЯ НА РЕЙС ЗАКАНЧИВАЕТСЯ ЗА 45 МИНУТ ДО ОТПРАВЛЕНИЯ, ЕСЛИ ИНОЕ НЕ УСТАНОВЛЕНО ПЕРЕВОЗЧИКОМ/CHECK-IN IS COMPLETED 45 MINUTES PRIORE TO DEPARTURE UNLESS THE CARRIER ANNOUNCES OTHERWISE; ПЕРЕВОЗЧИК ОБЯЗУЕТСЯ ПРИНЯТЬ ВСЕ ЗАВИСЯЩИЕ ОТ НЕГО МЕРЫ, ЧТОБЫ ПЕРЕВЕЗТИ ПАССАЖИРА И БАГАЖ В РАЗУМНЫЕ СРОКИ. ВРЕМЯ, УКАЗАННОЕ В РАСПИСАНИИ ИЛИ ДРУГИХ ДОКУМЕНТАХ, НЕ ГАРАНТИРУЕТСЯ И НЕ ЯВЛЯЕТСЯ СОСТАВНОЙ ЧАСТЬЮ НАСТОЯЩЕГО ДОГОВОРА. ПЕРЕВОЗЧИК МОЖЕТ БЕЗ ПРЕДУПРЕЖДЕНИЯ ПЕРЕДАТЬ ПАССАЖИРА ДЛЯ ПЕРЕВОЗКИ ДРУГОМУ ПЕРЕВОЗЧИКУ, ЗАМЕНИТЬ ВОЗДУШНОЕ СУДНО, ИЗМЕНИТЬ ИЛИ ОТМЕНИТЬ ПОСАДКУ В ПУНКТАХ, УКАЗАННЫХ В БИЛЕТЕ, ЕСЛИ ЭТО НЕОБХОДИМО. РАСПИСАНИЕ МОЖЕТ БЫТЬ ИЗМЕНЕНО БЕЗ ПРЕДУПРЕЖДЕНИЯ ПАССАЖИРА. ПЕРЕВОЗЧИК НЕ НЕСЕТ ОТВЕТСТВЕННОСТИ ЗА ОБЕСПЕЧЕНИЕ СТЫКОВОК РЕЙСОВ. CARRIER UNDERTAKES TO USE ITS BEST EFFORTS TO CARRY THE PASSENGER AND BAGGAGE WITH REASONABLE DISPATCH. TIMES SHOWN IN TIMETABLES OR ELSEWHERE ARE NOT GUARANTEED AND FORM NO PART OF THIS CONTRACT. CARRIER MAY WITHOUT NOTICE SUBSTITUTE ALTERNATE CARRIERS OR AIRCRAFT, AND MAY ALTER OR OMIT STOPPING PLACES SHOWN ON THE TICKET IN CASE OF NECESSITY. SCHEDULES ARE SUBJECT TO CHANGE WITHOUT NOTICE. CARRIER ASSUMES NO RESPONSIBILITY FOR MAKING CONNECTIONS.");
                        doc.Add(tSmall);
                    }
                    else
                    {
                        p1.Add("ОШИБКА / ERROR");
                        p2.Add("Для данного заказа электронный билет недоступен / Electronic ticket is not available");
                        doc.Add(p1);
                        doc.Add(p2);

                    }
                }
                else
                {
                    p1.Add("ОШИБКА / ERROR");
                    p2.Add("Ошибка в переданных параметрах / Error in parameters passed");
                    doc.Add(p1);
                    doc.Add(p2);
                }
            }
            else
            {
                p1.Add("ОШИБКА / ERROR");
                p2.Add("Документ не найден / Document not found");
                doc.Add(p1);
                doc.Add(p2);
            }

            doc.Close();
            Response.Write(doc);
        }
    }
}