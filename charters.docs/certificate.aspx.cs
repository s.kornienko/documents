﻿using System;
using System.Web;
using System.Data;

using charters.docs.INCLUDE.dataTableAdapters;

using iTextSharp.text;
using iTextSharp.text.pdf;

namespace charters.docs
{
    public partial class sertificate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string hash = Request.QueryString["hash"];
            string group = Request.QueryString["group"];
            int bookID = 0, bookStatus = 0;

            bool isGroup = (group == "1") ? true : false;

            _spCheckBookHash2_TA table = new _spCheckBookHash2_TA();
            var Rs = table.GetData(hash, isGroup);

            BaseFont bf = BaseFont.CreateFont(Server.MapPath("/resources/arial.ttf"), BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            Font fontsText = new Font(bf, 12, Font.NORMAL, new CMYKColor(0, 0, 0, 255)); // Черный 12 px
            Font fontsText10 = new Font(bf, 10, Font.NORMAL, new CMYKColor(0, 0, 0, 255)); // Черный 10 px
            Font fontsTextSmallBold = new Font(bf, 9, Font.BOLD, new CMYKColor(0, 0, 0, 255)); // Черный 9 px жирный
            Font fontsTextSmall = new Font(bf, 9, Font.NORMAL, new CMYKColor(0, 0, 0, 150)); // Серый 9 px
            Font fontsParagraph = new Font(bf, 14, Font.BOLD, new CMYKColor(0, 0, 0, 255)); // Черный 12 px жирный

            Paragraph p1 = new Paragraph("", fontsParagraph);
            Paragraph p2 = new Paragraph("", fontsText);

            p1.Alignment = Element.ALIGN_CENTER;
            p2.Alignment = Element.ALIGN_CENTER;

            p1.SpacingAfter = 10;
            p2.SpacingAfter = 10;

            Phrase t1 = new Phrase("", fontsText);
            Phrase tSmall = new Phrase("", fontsTextSmall);

            var doc = new Document(PageSize.A4);
            PdfWriter.GetInstance(doc, Response.OutputStream);
            doc.Open();

            if (Rs.Rows.Count > 0)
            {
                bookID = (int)Rs.Rows[0]["BID"];
                bookStatus = Convert.ToInt32(Rs.Rows[0]["BStatusID"]);

                if (bookID > 0 && (bookStatus == 2 || bookStatus == 3))
                {
                    // Пишем информацию о скачивании файла
                    new TAQuery()._spAddDownloadDocInfo(bookID, 1, 0, HttpContext.Current.Request.UserHostAddress);
                    Response.AppendHeader("content-disposition", "attachment; filename=certificate_" + bookID.ToString() + ".pdf");

                    p1.Add("СЕРТИФИКАТ НА АВИАБИЛЕТ");
                    p2.Add("CERTIFICATE");
                    doc.Add(p1);
                    doc.Add(p2);

                    // Добавляем таблицу с данными по брони
                    string bookNumber = "", bookDate = "", acName = "";

                    _spGetBookPdfInfoCertificate_TA tableBook = new _spGetBookPdfInfoCertificate_TA();
                    var RsBook = tableBook.GetData(bookID);
                    if (RsBook.Rows.Count > 0)
                    {
                        bookNumber = (string)RsBook.Rows[0]["BNumber"];
                        bookDate = (string)RsBook.Rows[0]["bookDate"];
                        acName = (string)RsBook.Rows[0]["acName"];
                    }

                    PdfPTable tbl = new PdfPTable(2);

                    tbl.DefaultCell.Border = Rectangle.NO_BORDER;
                    tbl.WidthPercentage = 99;
                    tbl.SpacingBefore = 20;
                    tbl.SpacingAfter = 20;

                    tbl.AddCell(new Phrase("НОМЕР ЗАКАЗА / BILLING NUMBER:", fontsText10));
                    tbl.AddCell(new Phrase(bookNumber, fontsText10));

                    tbl.AddCell(new Phrase("ДАТА ОФОРМЛЕНИЯ / DATE ISSUED:", fontsText10));
                    tbl.AddCell(new Phrase(bookDate, fontsText10));

                    tbl.AddCell(new Phrase("АВИАКОМПАНИЯ / CARRIER:", fontsText10));
                    tbl.AddCell(new Phrase(acName, fontsText10));

                    doc.Add(tbl);

                    // Добавляем таблицу с данными по пассажирам
                    PdfPTable tblPass = new PdfPTable(3);
                    tblPass.WidthPercentage = 99;
                    tblPass.SpacingAfter = 20;
                    tblPass.DefaultCell.Padding = 5;

                    tblPass.AddCell(new Phrase("ФАМИЛИЯ / NAME", fontsTextSmallBold));
                    tblPass.AddCell(new Phrase("ДОКУМЕНТ / DOCUMENT", fontsTextSmallBold));
                    tblPass.AddCell(new Phrase("ДАТА РОЖДЕНИЯ / BIRTH DATE", fontsTextSmallBold));

                    if (isGroup)
                    {
                        _spGetBookGroupPassengerListSetificate_TA tablePass = new _spGetBookGroupPassengerListSetificate_TA();
                        var RsPass = tablePass.GetData(bookID);
                        foreach (DataRow row in RsPass.Rows)
                        {
                            tblPass.AddCell(new Phrase(row["fio"].ToString(), fontsText10));
                            tblPass.AddCell(new Phrase(row["CPDocNo"].ToString(), fontsText10));
                            tblPass.AddCell(new Phrase(row["birthDate"].ToString(), fontsText10));
                        }
                    }
                    else
                    {
                        _spGetBookPassengerListSetificate_TA tablePass = new _spGetBookPassengerListSetificate_TA();
                        var RsPass = tablePass.GetData(bookID);
                        foreach (DataRow row in RsPass.Rows)
                        {
                            tblPass.AddCell(new Phrase(row["fio"].ToString(), fontsText10));
                            tblPass.AddCell(new Phrase(row["CPDocNo"].ToString(), fontsText10));
                            tblPass.AddCell(new Phrase(row["birthDate"].ToString(), fontsText10));
                        }
                    }

                    doc.Add(tblPass);

                    // Добавляем таблицу с данными по перелету
                    PdfPTable tblFlight = new PdfPTable(4);
                    tblFlight.WidthPercentage = 99;
                    tblFlight.SpacingAfter = 20;
                    tblFlight.DefaultCell.Padding = 5;

                    tblFlight.AddCell(new Phrase("ДАТА / РЕЙС / КЛАСС\nDATE / FLIGHT / CLASS", fontsTextSmallBold));
                    tblFlight.AddCell(new Phrase("ВЫЛЕТ\nDEPARTURE", fontsTextSmallBold));
                    tblFlight.AddCell(new Phrase("ПРИЛЁТ\nARRIVAL", fontsTextSmallBold));
                    tblFlight.AddCell(new Phrase("БАГАЖ\nBAGGAGE", fontsTextSmallBold));

                    _spGetBookFlightSetificate_TA tableFlight = new _spGetBookFlightSetificate_TA();
                    var RsFlight = tableFlight.GetData(bookID);
                    foreach (DataRow row in RsFlight.Rows)
                    {
                        string dateDep = (string)row["dateDep"];
                        string cityDep = (string)row["cityDep"];
                        string portDep = (string)row["portDep"];
                        string portDepCode = (string)row["portDepCode"];
                        string terminalDep = (string)row["terminalDep"];

                        string cityArr = (string)row["cityArr"];
                        string portArr = (string)row["portArr"];
                        string portArrCode = (string)row["portArrCode"];
                        string terminalArr = (string)row["terminalArr"];

                        string timeDep = (string)row["timeDep"];
                        string timeArr = (string)row["timeArr"];

                        string flName = (string)row["airCompany"] + " " + (string)row["flightNumber"];
                        string bortName = (string)row["CFT_Name"];
                        string classCode = (string)row["classCode"];
                        string baggage = row["baggage"].ToString();

                        cityDep += (portDep != cityDep) ? ". " + portDep : "";
                        cityDep += " (" + portDepCode + ")";
                        cityDep += (terminalDep.Length > 0) ? ". Терминал " + terminalDep : "";

                        cityArr += (portArr != cityArr) ? ". " + cityArr : "";
                        cityArr += " (" + portArrCode + ")";
                        cityArr += (terminalArr.Length > 0) ? ". Терминал " + terminalArr : "";

                        tblFlight.AddCell(new Phrase(dateDep + "\n" + flName + "\n" + bortName + "\n" + classCode, fontsText10));
                        tblFlight.AddCell(new Phrase(cityDep + "\n" + timeDep, fontsText10));
                        tblFlight.AddCell(new Phrase(cityArr + "\n" + timeArr, fontsText10));
                        tblFlight.AddCell(new Phrase(baggage + "K", fontsText10));
                    }

                    doc.Add(tblFlight);

                    // Добавляем общий текст
                    tSmall.Clear();
                    tSmall.Add("Данный сертификат подтверждает право на получение маршрутной квитанции электронного авиабилета. Сертификат не является билетом и недействителен для перелета." +
                        "Пассажир допускается к полету только после оформления электронного билета. Электронный билет выписывается накануне вылета до 22 ч. по московскому времени." + 
                        "Перед вылетом получите электронный билет. \n\n");

                    tSmall.Add("This sertificate confirms you right to receive an E-ticket itinerary receipt. Sertificate is not a ticket and it is not valid for journey." +
                        "For getting on board E-ticket is required. E-ticket will be issued till 22-00 the day before departure (Moscow time zone)." +
                        "Please get your E-ticket before journey.");

                    doc.Add(tSmall);
                }
                else
                {
                    p1.Add("ОШИБКА / ERROR");
                    p2.Add("Для данного заказа электронный билет недоступен / Electronic ticket is not available");
                    doc.Add(p1);
                    doc.Add(p2);

                }
            }
            else
            {
                p1.Add("ОШИБКА / ERROR");
                p2.Add("Документ не найден / Document not found");
                doc.Add(p1);
                doc.Add(p2);
            }

            doc.Close();
            Response.Write(doc);
        }
    }
}