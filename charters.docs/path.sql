if (object_id('dbo._spCheckBookHash2') is not null) and (objectproperty(object_id('dbo._spCheckBookHash2'), 'IsProcedure') = 1)
drop procedure dbo._spCheckBookHash2
GO

CREATE PROCEDURE [dbo].[_spCheckBookHash2]
@Hash nvarchar(50)='',
@isGroup bit=0
AS
	if @isGroup = 0
		select b.BID, b.BStatusID,
			isNull((select top 1 isWork from _TInvoices where BookID = b.BID), 0) as 'isInvoice' 
		from TBook b
		where b.BID_HASH = @Hash
	else
		select ID as 'BID', StatusID as 'BStatusID', 0 as 'isInvoice'
		from TBookGroup 
		where BID_HASH = @Hash
GO

if (object_id('dbo._spGetBookPdfInfoTicket') is not null) and (objectproperty(object_id('dbo._spGetBookPdfInfoTicket'), 'IsProcedure') = 1)
drop procedure dbo._spGetBookPdfInfoTicket
GO

CREATE PROCEDURE [dbo].[_spGetBookPdfInfoTicket]
@passID int=0,
@bookID int=0
AS
	select isNull(b.BNumber, '') as 'BNumber', isNull(cp.PNR, '') as 'PNR', isNull(cp.ticketNumber, '') as 'ticketNumber', dbo.DateToStr(b.ticketDate, 104) as 'ticketDate', 
		(select top 1 ac.ARC_NAME + ' (' + ac.ARC_CODE + ')' from TBookCharterFlights f inner join TrAIRCOMPANY ac on f.airCompany = ac.ARC_CODE 
		where f.BookID = b.BID and f.isBack = 0 order by f.ID) as 'acName',
		isNull(Upper(cp.CPFName), '') as 'fName', isNull(Upper(cp.CPLName), '') as 'lName', isNull(Upper(cp.CPMName), '') as 'mName', isNull(Upper(cp.CPDocNo), '') as 'docNo'
	from TBook b
		inner join TBookCharterPerson cp on cp.CPBookID = b.BID
	where cp.CPID = @passID and b.BID = @bookID
GO

ALTER PROCEDURE [dbo].[_spGetBookFlightSetificate]
@BookID int=0
AS
	select isNull(fl.airCompany, '') as 'airCompany', fl.flightNumber, 
		iif(len(fl.classCode) > 1, fl.classCode, (select CName from TrClass where iata = fl.classCode)) as 'classCode',
		dbo.DateToStr(fl.DateDep, 104) as 'dateDep', fl.timeDep, fl.timeArr,
		isNull(bort.CFT_NAME_ENG, '') as 'CFT_NAME_ENG', isNull(bort.CFT_NAME, '') as 'CFT_NAME', 
		bort.CFT_CODE,
		isNull(ac.ARC_NAME, '') as 'ARC_NAME', cityFrom.CityName as 'cityDep', cityTo.CityName as 'cityArr',
		iif(len(fl.terminalDep) = 0, portFrom.RT_NAME, portFrom.RT_NAME + ' (терминал ' + fl.terminalDep + ')') as 'portDep',
		iif(len(fl.terminalArr) = 0, portTo.RT_NAME, portTo.RT_NAME + ' (терминал ' + fl.terminalArr + ')') 'portArr',
		fl.terminalArr, fl.terminalDep, portFrom.RT_CODE as 'portDepCode', portTo.RT_CODE as 'portArrCode'
		,iif(fl.airCompany = 'DP', '10', dbo.IntToStr(cl.baggage)) as 'baggage'
	from TBook book
		inner join TBookCharterFlights fl on book.BID = fl.BookID
		left outer join TrAIRCRAFT bort on fl.bort = bort.CFT_CODE
		left outer join TrAIRCOMPANY ac on fl.airCompany = ac.ARC_CODE
		inner join TrAIRPORT portFrom on fl.PortDep = portFrom.RT_CODE
		inner join TrCity cityFrom on portFrom.CTY_ID = cityFrom.CityID
		inner join TrAIRPORT portTo on fl.PortArr = portTo.RT_CODE
		inner join TrCity cityTo on portTo.CTY_ID = cityTo.CityID
		left outer join TrClass cl on cl.iata = fl.classCode
	where book.BID = @BookID
GO

/**********************************/
if (object_id('dbo._spGetBookPdfInfoCertificate') is not null) and (objectproperty(object_id('dbo._spGetBookPdfInfoCertificate'), 'IsProcedure') = 1)
drop procedure dbo._spGetBookPdfInfoCertificate
GO

CREATE PROCEDURE [dbo].[_spGetBookPdfInfoCertificate]
@bookID int=0
AS
	select isNull(b.BNumber, '') as 'BNumber', dbo.DateToStr(b.BDate, 104) as 'bookDate', 
		(select top 1 ac.ARC_NAME + ' (' + ac.ARC_CODE + ')' from TBookCharterFlights f inner join TrAIRCOMPANY ac on f.airCompany = ac.ARC_CODE 
		where f.BookID = b.BID and f.isBack = 0 order by f.ID) as 'acName'
	from TBook b
	where b.BID = @bookID
GO
