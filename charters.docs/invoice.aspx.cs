﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
//using System.Web.UI.WebControls;
using System.Data;

using charters.docs.INCLUDE.dataTableAdapters;

using iTextSharp.text;
using iTextSharp.text.pdf;

namespace charters.docs
{
    public partial class invoice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string hash = Request.QueryString["hash"];
            string group = Request.QueryString["group"];
            int bookID = 0, bookStatus = 0;
            bool isInvoice = false;
            bool isGroup = (group == "1") ? true : false;

            _spCheckBookHash2_TA table = new _spCheckBookHash2_TA();
            var Rs = table.GetData(hash, isGroup);

            BaseFont bf = BaseFont.CreateFont(Server.MapPath("/resources/arial.ttf"), BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            Font fontsText = new Font(bf, 9, Font.NORMAL, new CMYKColor(0, 0, 0, 255)); // Черный 9 px
            Font fontsText10 = new Font(bf, 10, Font.NORMAL, new CMYKColor(0, 0, 0, 255)); // Черный 10 px
            Font fontsTextSmallBold = new Font(bf, 9, Font.BOLD, new CMYKColor(0, 0, 0, 255)); // Черный 9 px жирный
            Font fontsTextSmall = new Font(bf, 9, Font.NORMAL, new CMYKColor(0, 0, 0, 150)); // Серый 9 px
            Font fontsParagraph = new Font(bf, 14, Font.BOLD, new CMYKColor(0, 0, 0, 255)); // Черный 12 px жирный

            Paragraph p1 = new Paragraph("", fontsParagraph);
            Paragraph p2 = new Paragraph("", fontsText);

            //p1.Alignment = Element.ALIGN_CENTER;
            p2.Alignment = Element.ALIGN_CENTER;

            p1.SpacingAfter = 10;
            p2.SpacingAfter = 10;

            Phrase t1 = new Phrase("", fontsText);
            Phrase tSmall = new Phrase("", fontsTextSmall);

            var doc = new Document(PageSize.A4);
            PdfWriter.GetInstance(doc, Response.OutputStream);
            doc.Open();

            if (Rs.Rows.Count > 0)
            {
                bookID = (int)Rs.Rows[0]["BID"];
                bookStatus = Convert.ToInt32(Rs.Rows[0]["BStatusID"]);
                isInvoice = (bool)Rs.Rows[0]["isInvoice"];

                // Пишем информацию о скачивании файла
                new TAQuery()._spAddDownloadDocInfo(bookID, 3, 0, HttpContext.Current.Request.UserHostAddress);
                Response.AppendHeader("content-disposition", "attachment; filename=invoice_" + bookID.ToString() + ".pdf");

                t1.Add("Внимание! Оплата данного счета означает согласие с условиями поставки товара. Уведомление об оплате обязательно, в противном случае не гарантируется " +
                    "наличие товара на складе. Товар отпускается по факту прихода денег на р/с Поставщика, самовывозом, при наличии доверенности и паспорта");
                doc.Add(t1);

                // Выводим шапку
                int invNumber = 0, invResID = 0;
                string invDate = "", invBank = "", invReciv = "", invBuyer = "", invINN = "", invKPP = "", invBIK = "", invKorrAcc = "", invRasAcc = "";
                string invDirector = "", invAccount = "", invAddr = "", allTextSumm = "";

                _spGetInvoiceInfoForPDF_TA table1 = new _spGetInvoiceInfoForPDF_TA();
                var Rs1 = table1.GetData(bookID);
                if (Rs1.Rows.Count > 0)
                {
                    invNumber = (int)Rs1.Rows[0]["ID"];
                    invDate = (string)Rs1.Rows[0]["Date"];
                    invBank = (string)Rs1.Rows[0]["BankName"];
                    invReciv = (string)Rs1.Rows[0]["RecieverName"];
                    invBuyer = (string)Rs1.Rows[0]["BuyerName"];
                    invINN = (string)Rs1.Rows[0]["INN"];
                    invKPP = (string)Rs1.Rows[0]["KPP"];
                    invBIK = (string)Rs1.Rows[0]["BIK"];
                    invKorrAcc = (string)Rs1.Rows[0]["Account1"];
                    invRasAcc = (string)Rs1.Rows[0]["Account2"];
                    invDirector = (string)Rs1.Rows[0]["DirectorName"];
                    invAccount = (string)Rs1.Rows[0]["accountantName"];
                    invAddr = (string)Rs1.Rows[0]["addr"];
                    allTextSumm = (string)Rs1.Rows[0]["allSumm"];
                    invResID = (int)Rs1.Rows[0]["receiverID"];
                }

                PdfPTable tbl = new PdfPTable(4);

                tbl.WidthPercentage = 99;
                tbl.SpacingBefore = 10;
                tbl.SpacingAfter = 20;

                var cell = new PdfPCell(new Phrase(invBank, fontsText10));
                cell.Colspan = 2;
                cell.BorderWidthBottom = 0;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("БИК", fontsText10));
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase(invBIK, fontsText10));
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("Банк получателя", fontsText10));
                cell.Colspan = 2;
                cell.BorderWidthTop = 0;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("Сч. №", fontsText10));
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase(invKorrAcc, fontsText10));
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("ИНН " + invINN, fontsText10));
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("КПП " + invKPP, fontsText10));
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("Сч. №", fontsText10));
                cell.Rowspan = 3;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase(invRasAcc, fontsText10));
                cell.Rowspan = 3;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase(invReciv, fontsText10));
                cell.Colspan = 2;
                cell.BorderWidthBottom = 0;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("Получатель", fontsText10));
                cell.Colspan = 2;
                cell.BorderWidthTop = 0;
                tbl.AddCell(cell);

                doc.Add(tbl);

                // Вывод заголовка счета
                p1.Clear();
                p1.Add("Счет на оплату № " + invNumber + " от " + invDate);
                doc.Add(p1);

                var line1 = new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1);
                doc.Add(new Chunk(line1));

                // Вывод таблицы о покупателе
                tbl = new PdfPTable(2);
                tbl.WidthPercentage = 99;
                tbl.SpacingBefore = 5;
                tbl.SpacingAfter = 10;
                tbl.SetWidths(new float[] { 20, 80 });

                cell = new PdfPCell(new Phrase("Поставщик:", fontsText10));
                cell.BorderWidth = 0;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase(invReciv + ", ИНН " + invINN + ", КПП " + invKPP + ", " + invAddr, fontsTextSmallBold));
                cell.BorderWidth = 0;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("Покупатель:", fontsText10));
                cell.BorderWidth = 0;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase(invBuyer, fontsTextSmallBold));
                cell.BorderWidth = 0;
                tbl.AddCell(cell);

                doc.Add(tbl);

                // Вывод таблицы с услугами
                int sCount = 0, sNum = 0;
                decimal sPrice = 0, allSumm = 0;
                string sFlight = "", sFromD = "", sBackD = "";

                tbl = new PdfPTable(6);
                tbl.WidthPercentage = 99;
                tbl.SpacingBefore = 10;
                tbl.SpacingAfter = 20;
                tbl.SetWidths(new float[] { 5, 65, 5, 5, 10, 10 });

                cell = new PdfPCell(new Phrase("№", fontsTextSmallBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("Товары (работы, услуги)", fontsTextSmallBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("Кол-во", fontsTextSmallBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("Ед.", fontsTextSmallBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("Цена", fontsTextSmallBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("Сумма", fontsTextSmallBold));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                tbl.AddCell(cell);

                sNum = 0;
                allSumm = 0;

                _spGetInvoiceDetalInfoForPDF_TA table2 = new _spGetInvoiceDetalInfoForPDF_TA();
                var Rs2 = table2.GetData(bookID);
                foreach (DataRow row in Rs2.Rows)
                {
                    sNum++;
                    sCount = (int)row["productQuantity"];
                    sPrice = (decimal)row["price"];
                    sFlight = (string)row["flight"];
                    sFromD = (string)row["fromDate"];
                    sBackD = (string)row["backDate"];

                    allSumm += sPrice * sCount;

                    if (sBackD.Length > 0) sFromD += " - " + sBackD;

                    tbl.AddCell(new PdfPCell(new Phrase(sNum.ToString(), fontsText10)));
                    tbl.AddCell(new PdfPCell(new Phrase("заявка " + bookID + " " + sFlight + " " + sFromD, fontsText10)));
                    tbl.AddCell(new PdfPCell(new Phrase(sCount.ToString(), fontsText10)));
                    tbl.AddCell(new PdfPCell(new Phrase("чел", fontsText10)));

                    cell = new PdfPCell(new Phrase(Math.Round(sPrice, 2).ToString(), fontsText10));
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tbl.AddCell(cell);

                    cell = new PdfPCell(new Phrase(Math.Round(sPrice * sCount, 2).ToString(), fontsText10));
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    tbl.AddCell(cell);
                }
                cell = new PdfPCell(new Phrase("Итого:", fontsTextSmallBold));
                cell.Colspan = 5;
                cell.BorderWidth = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase(Math.Round(allSumm, 2).ToString(), fontsTextSmallBold));
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidth = 0;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("Без налога НДС", fontsTextSmallBold));
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidth = 0;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("-", fontsTextSmallBold));
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidth = 0;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("Всего к оплате: ", fontsTextSmallBold));
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidth = 0;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase(Math.Round(allSumm, 2).ToString(), fontsTextSmallBold));
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidth = 0;
                tbl.AddCell(cell);

                doc.Add(tbl);

                // Вывод информации по услугам
                t1.Clear();
                t1.Add("Всего наименований " + sNum + ", на сумму " + Math.Round(allSumm, 2));
                doc.Add(t1);
                doc.Add(new Paragraph(allTextSumm, fontsTextSmallBold));
                doc.Add(new Chunk(line1));

                // Вывод данных о директоре и бухгалтере
                tbl = new PdfPTable(4);
                tbl.WidthPercentage = 99;
                tbl.SpacingBefore = 20;
                tbl.SpacingAfter = 20;

                cell = new PdfPCell(new Phrase("Руководитель", fontsTextSmallBold));
                cell.BorderWidth = 0;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase(invDirector, fontsText10));
                cell.BorderWidthLeft = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthRight = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase("Бухгалтер", fontsTextSmallBold));
                cell.BorderWidth = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                tbl.AddCell(cell);

                cell = new PdfPCell(new Phrase(invAccount, fontsText10));
                cell.BorderWidthLeft = 0;
                cell.BorderWidthTop = 0;
                cell.BorderWidthRight = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                tbl.AddCell(cell);

                doc.Add(tbl);

                Image img = Image.GetInstance(Server.MapPath("/resources/stamp" + invResID + ".png"));
                Image img2 = Image.GetInstance(Server.MapPath("/resources/podp" + invResID + ".png"));

                img.ScaleToFit(110, 110);
                img.SetAbsolutePosition(235, 270);
                img2.ScalePercent(30);
                img2.SetAbsolutePosition(100, 300);

                doc.Add(img);
                doc.Add(img2);

                img2.SetAbsolutePosition(400, 310);
                doc.Add(img2);

            }
            else
            {
                p1.Add("ОШИБКА / ERROR");
                p2.Add("Документ не найден / Document not found");
                doc.Add(p1);
                doc.Add(p2);
            }

            doc.Close();
            Response.Write(doc);
        }
    }
}